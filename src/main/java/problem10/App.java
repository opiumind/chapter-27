package problem10;

/**
 * **27.10 (Compare MyHashSet and MyArrayList ) MyArrayList is defined in Listing
 24.3. Write a program that generates 1000000 random double values between 0
 and 999999 and stores them in a MyArrayList and in a MyHashSet . Generate
 a list of 1000000 random double values between 0 and 1999999 . For each num-
 ber in the list, test if it is in the array list and in the hash set. Run your program to
 display the total test time for the array list and for the hash set.
 *
 */
public class App
{
    public static void main( String[] args ) {
        int range = 100000;
        Double[] numbers = new Double[range];
        Double[] numbersForTest = new Double[range];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Math.random() * 999999;
            numbersForTest[i] = Math.random() * 1999999;
        }

        MyArrayList<Double> myArrayList = new MyArrayList<>(numbers, range);
        MyHashSet<Double> myHashSet = new MyHashSet<>(range);
        myHashSet.addAll(numbers);

        testContainsForArrayListAndHashSet(numbersForTest, myArrayList, myHashSet);
    }

    public static void testContainsForArrayListAndHashSet(Double[] numbersForTest, MyArrayList<Double> myArrayList,
                                                          MyHashSet<Double> myHashSet) {
        long timeOfMyArrayList;
        long timeOfMyHashSet;

        long initialTime = System.currentTimeMillis();
        for (int i = 0; i < numbersForTest.length; i++) {
            myArrayList.contains(numbersForTest[i]);
        }
        timeOfMyArrayList = System.currentTimeMillis() - initialTime;
        initialTime = System.currentTimeMillis();
        for (int i = 0; i < numbersForTest.length; i++) {
            myHashSet.contains(numbersForTest[i]);
        }
        timeOfMyHashSet = System.currentTimeMillis() - initialTime;
        System.out.println("Total test time for MyArrayList: " + timeOfMyArrayList +
                "\nTotal test time for myHashSet: " + timeOfMyHashSet);
    }
}
